#!/bin/bash
# Можно скопировать этот файл в проект, тогда папка будет прописана автоматически

PROJECT_PATH=$(pwd)
PROJECT_NAME=$(basename $PWD)
DEFAULT_DOMAIN="fablab61.ru"
DEFAULT_PORT="8080"
cp example.cnf production.conf

echo "Введите ПОЛНЫЙ путь к корневой папке проекта Mojo::Lite. По умолчанию: [ $PROJECT_PATH ]"
read  path
if [[ $path = "" ]]; then 
    path=$PROJECT_PATH
fi
sed -i "s|root /home/test_dir*|root $path|" production.conf


echo "Введите имя upstream. По умолчанию: [ $PROJECT_NAME ]"
read  upstream
if [[ $upstream = "" ]]; then 
    upstream=$PROJECT_NAME
fi
sed -i "s|upstream myapp*|upstream $upstream|" production.conf
sed -i "s|proxy_pass http://myapp*|proxy_pass http://$upstream|" production.conf


echo "Перед вами список открытых портов: \n"
netstat -ntulp | grep LISTEN
echo "Введите номер порта.  По умолчанию: 8080 \n"
read  port
if [[ $port = "" ]]; then 
    port=$DEFAULT_PORT
fi
sed -i "s|server 127.0.0.1:8080*|server 127.0.0.1:$port|" production.conf


echo "Введите имя домена или поддомена. Домены которые уже заняты:"
find /etc/nginx/sites-available -type f -print0 | xargs -0 egrep '^(\s|\t)*server_name'
echo "По умолчанию: $DEFAULT_DOMAIN \n"
read  domain
if [[ $domain = "" ]]; then 
    domain=$DEFAULT_DOMAIN
fi
sed -i "s|server_name example.loc www.example.loc*|server_name $domain www.$domain|" production.conf


echo "############# CONFIG GENERATED : "
cat production.conf
echo "############# "


sudo cp production.conf /etc/nginx/sites-available/$domain
rm production.conf
cd /etc/nginx/sites-enabled
sudo ln -s ../sites-available/$domain
sudo nginx -s reload


echo "Changing hypnotoad port in main app config... "
########## Change hypnotoad port in main app config #############
cd $path
C=$(find . -name '*.conf')
# echo $C # e.g. ./myapp.conf

# REPLACEMENT_KEY="project"
# sed -e "s/$REPLACEMENT_KEY/$NEW_VALUE/" $PATH_TO_R_CONFIG_FILE > "$PWD/deploy/$A.local.conf"

# sed -i "s|listen  => ['http://\*:*|listen  => ['http://*:$port'],|" $C

# # sed -i "s|listen  => ['http://\*:*|listen  => ['http://*:$port'],|" $C
# cat $C

# sed -i -- 's/listen  => \[\http://\*:\*\/bar\   t/g/' $C



########## Find main app script and run it in main app config #############
# R=$(find . -name '*.pl')
# hypnotoad $R


echo "Deploy finished! Не забудьте перейти в $PROJECT_PATH и :
1) Установить $port порт hypnotoad в конфиге приложения ($C)
2) запустить hypnotoad
3) добавить домен на DNS сервер"
exit 0

# Вначале ещё можно тип конфига указывать (http/https)
# echo "Введите тип проекта. \n1 - http\n2 - https\n :"
# read project_type
# case $project_type in
#         1)
#                 echo "Вы выбрали http"
#                 ;;
#         2)
#                 echo "Вы выбрали https"
#                 ;;
#         *)
#                 echo "Выбор не распознан"
#                 break
#                 ;;
# esac